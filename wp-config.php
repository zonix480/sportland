<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sportland' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=46>0DRgr[Q8mHQ]Zq^QQ8V4o@$YY0<6QVg>$AHRY|A%d QDpbU9[Zx.F>RCk#l!' );
define( 'SECURE_AUTH_KEY',  '?}knay~*N$.l5y.41@<0cZbvwlfR4VD;DON3~`db=$v*Tw#Hr5`-$D@#e>7zcQT:' );
define( 'LOGGED_IN_KEY',    '3oGN4c@={e>zzn5/OVSW5TDN@Rw-6{/%qCLZ$=vL5])pB|j+8LD6hMwG{LAICd@x' );
define( 'NONCE_KEY',        'G=++j.@K){+{4$]o(Gn{<A6!(VZGHOW/(e>o |!C4?]cN&3`-DhJ*CIRBiMg@re8' );
define( 'AUTH_SALT',        'inQ-Q0.|[_1S)QfY!_j2nBNDQcp|<jAV{Z!I|0_f0|g-|/UGmvg7>kDp*f:;ePWR' );
define( 'SECURE_AUTH_SALT', '_XPOGq!8:&[Be1Zo6m!N+Ws9wo.Tm{/`i_-P<qk0/-J[ypMV91<(5+Frq$t^PJze' );
define( 'LOGGED_IN_SALT',   '[?Ag0FZ%*U*l`FD]e%.;#;Hh3zSHx2xRSBBL$yq/ep2Fy`px-Q!?##KF7Z5*6,<#' );
define( 'NONCE_SALT',       'x>ebS~Vv^L Z~lJ|UFrXD.D~d7L@m~:$p%BEi>CB0rY~S@B2+I=T@zh1-b2#~;$6' );
define('FS_METHOD','direct');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_sportland';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
